#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main()
{
	//Creación del fichero, el puntero a la memoria y el puntero para la proyección.
	int file;
	DatosMemCompartida* pMemComp;
	char* proyeccion;

	//Apertura del fichero:
	file=open("/tmp/datosBot.txt",O_RDWR);

	//Proyección del fichero:
	proyeccion=(char*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);

	//Cierre del fichero:
	close(file);

	//Se apunta el puntero de Datos a la proyección del fichero en memoria:
	pMemComp=(DatosMemCompartida*)proyeccion;

	//Condición de salida
	int salir=0;

	//Acciones de control de la raqueta:
	while(salir==0)
	{
		if (pMemComp->accion==5)
		{
			salir=1;
		}
		usleep(25000);  
		float posRaqueta;
		//float posRaqueta2;
		posRaqueta=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
		//posRaqueta2=((pMemComp->raqueta2.y2+pMemComp->raqueta2.y1)/2);
		if(posRaqueta<pMemComp->esfera.centro.y)
			pMemComp->accion=1;
		else if(posRaqueta>pMemComp->esfera.centro.y)
			pMemComp->accion=-1;
		else
			pMemComp->accion=0;
		
	}

	//Se desmonta la proyección de memoria:
	munmap(proyeccion,sizeof(*(pMemComp)));
}
